import React, { useEffect, useRef, useState } from "react";
import { CubeTextureLoader } from "three";
import { HDRCubeTextureLoader } from "three/examples/jsm/loaders/HDRCubeTextureLoader";
import { Experience } from "../Experience/Experience";
import { MyMesh } from "../store/modelSlice";

interface Props {
  target?: MyMesh;
  exp: Experience | null;
}

type EnvMapTarget = "scene" | "model";

const ChangeEnvMap = ({ target, exp }: Props) => {
  let [envMapTarget, setEnvMapTarget] = useState<EnvMapTarget>("scene");

  const removeModelEnvBtn = useRef<HTMLButtonElement>(null);
  const removeSceneEnvBtn = useRef<HTMLButtonElement>(null);
  const envTextureRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (target) {
      if (removeModelEnvBtn.current)
        removeModelEnvBtn.current.disabled = !Boolean(target.material.envMap);
    } else {
      if (removeModelEnvBtn.current) removeModelEnvBtn.current.disabled = true;
    }
  }, [target]);

  useEffect(() => {
    if (removeSceneEnvBtn.current)
      removeSceneEnvBtn.current.disabled = !Boolean(exp?.scene?.background);
  }, []);

  let loadEnvBtnHandler = (envTarget: EnvMapTarget) => {
    setEnvMapTarget(envTarget);
    envTextureRef.current?.click();
  };

  let loadEnvHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    let files = e.target.files;

    if (files?.length === 6) {
      let filesArr = Array.from(files).sort((a, b) =>
        a.name[1] < b.name[1] ? -1 : a.name[1] > b.name[1] ? 1 : 0
      );

      for (let k = 0; k < filesArr.length; k++) {
        if (k % 2) {
          let buff = filesArr[k];
          filesArr[k] = filesArr[k - 1];
          filesArr[k - 1] = buff;
        }
      }
      console.log(files);

      let loader: HDRCubeTextureLoader | CubeTextureLoader;

      if (
        filesArr.every((item) => item.name.split(".").pop() === "png") ||
        filesArr.every((item) => item.name.split(".").pop() === "jpg")
      ) {
        loader = new CubeTextureLoader();
      } else if (filesArr.every((item) => item.name.split(".").pop() === "hdr")) {
        loader = new HDRCubeTextureLoader();
      } else {
        console.error("Invalid data");
        return;
      }

      let links: string[] = [];

      filesArr.forEach((file) => {
        console.log(file.name);

        links.push(URL.createObjectURL(file));
      });

      let texture = loader.load(links, () => {});

      if (envMapTarget === "model" && target) {
        target.material.envMap = texture;
        // target.traverse((child) => {
        //   if (child instanceof Mesh && child.material instanceof MeshStandardMaterial) {
        //     child.material.needsUpdate = true;
        //   }
        // });
        // target.material.needsUpdate = true;
        if (removeModelEnvBtn.current) removeModelEnvBtn.current.disabled = false;
      } else if (envMapTarget === "scene" && exp?.scene) {
        exp.scene.background = texture;
        if (removeSceneEnvBtn.current) removeSceneEnvBtn.current.disabled = false;
      }
    }

    e.target.value = "";
  };

  let removeEnvMapHandler = (type: EnvMapTarget) => {
    if (type === "model" && target) {
      target.material.envMap = null;
      if (removeModelEnvBtn.current) removeModelEnvBtn.current.disabled = true;
    } else if (type === "scene" && exp?.scene) {
      console.log("!");

      exp.scene.background = null;
      if (removeSceneEnvBtn.current) removeSceneEnvBtn.current.disabled = true;
    }
  };

  // let remove

  return (
    <div className="texture">
      <span>EnvMap</span>
      <input
        multiple
        accept=".hdr, .jpg, .png"
        ref={envTextureRef}
        onChange={loadEnvHandler}
        type="file"
      />
      <button
        onClick={() => {
          loadEnvBtnHandler("model");
        }}
        disabled={!Boolean(target)}
      >
        Load Model EnvMap
      </button>
      <button
        onClick={() => {
          removeEnvMapHandler("model");
        }}
        disabled={!Boolean(target && target.material.envMap)}
        ref={removeModelEnvBtn}
      >
        Remove Model EnvMap
      </button>
      <hr style={{ margin: ".5rem 0" }} />
      <button
        onClick={() => {
          loadEnvBtnHandler("scene");
        }}
      >
        Load Scene EnvMap
      </button>
      <button
        onClick={() => {
          removeEnvMapHandler("scene");
        }}
        ref={removeSceneEnvBtn}
      >
        Remove Scene EnvMap
      </button>
    </div>
  );
};

export default ChangeEnvMap;
