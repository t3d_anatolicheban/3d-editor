import React, { memo, useEffect, useRef } from "react";
import { Experience } from "../Experience/Experience";
import { store } from "../store/store";

interface Props {
  onExpCreated(exp: Experience): void;
}

const Canvas = memo(({ onExpCreated }: Props) => {
  let canvasRef = useRef<HTMLCanvasElement>(null);
  let experience: Experience;

  useEffect(() => {
    experience = new Experience(canvasRef.current);
    onExpCreated(experience);
  }, []);

  return (
    <div className="canvas-wrapper">
      <canvas ref={canvasRef}></canvas>
    </div>
  );
});

export default Canvas;
