import React, { useRef, useEffect, useState } from "react";
import { MyMesh } from "../store/modelSlice";
import { RepeatWrapping, TextureLoader } from "three";

interface Props {
  target?: MyMesh;
}

type TextureType = "color" | "normal";

const ChangeModelTexture = ({ target }: Props) => {
  // const target = useSelector<RootState, MyMesh | undefined>(
  //   (state) => state.model.currTarget.target
  // );
  // let [currType] = useState<TextureType>();

  const textureRef = useRef<HTMLInputElement>(null);
  const normalRef = useRef<HTMLInputElement>(null);
  const removeTextureBtnRef = useRef<HTMLButtonElement>(null);
  const removeNormalBtnRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    if (target) {
      if (removeTextureBtnRef.current) {
        removeTextureBtnRef.current.disabled = !Boolean(target.material.map);
      }
      if (removeNormalBtnRef.current) {
        removeNormalBtnRef.current.disabled = !Boolean(target.material.normalMap);
      }
    } else {
      if (removeTextureBtnRef.current) removeTextureBtnRef.current.disabled = true;
      if (removeNormalBtnRef.current) removeNormalBtnRef.current.disabled = true;
    }
  }, [target]);

  let textureBtnHandler = (type: TextureType) => {
    if (!target) return;
    if (type === "color") {
      textureRef.current?.click();
    } else {
      normalRef.current?.click();
    }
  };

  let changeTextureHandler = (e: React.ChangeEvent<HTMLInputElement>, type: TextureType) => {
    if (!target) return;

    if (e.target.files?.length) {
      let link = URL.createObjectURL(e.target.files[0]);

      new TextureLoader().load(link, (texture) => {
        texture.wrapS = texture.wrapT = RepeatWrapping;

        if (type === "color") {
          target.material.map = texture;
        } else {
          target.material.normalMap = texture;
        }

        target.material.transparent = true;
        target.material.needsUpdate = true;

        if (removeTextureBtnRef.current)
          removeTextureBtnRef.current.disabled = !Boolean(target?.material.map);
        if (removeNormalBtnRef.current)
          removeNormalBtnRef.current.disabled = !Boolean(target?.material.normalMap);
      });
      setTimeout(() => {
        URL.revokeObjectURL(link);
      }, 10000);
    }
    e.target.value = "";
  };

  let removeTextureHandler = (type: TextureType) => {
    if (!target?.material.map && !target?.material.normalMap) return;

    if (type === "color") {
      target.material.map = null;
    } else {
      target.material.normalMap = null;
    }
    if (removeNormalBtnRef.current)
      removeNormalBtnRef.current.disabled = !Boolean(target?.material.normalMap);
    if (removeTextureBtnRef.current)
      removeTextureBtnRef.current.disabled = !Boolean(target?.material.map);
    target.material.needsUpdate = true;
  };

  return (
    <>
      <div className="texture">
        <span>Texture</span>
        <input
          accept=".jpg, .png"
          onChange={(e) => {
            changeTextureHandler(e, "color");
          }}
          ref={textureRef}
          type="file"
        />
        <button
          onClick={() => {
            textureBtnHandler("color");
          }}
          disabled={!Boolean(target)}
        >
          Load Texture
        </button>
        <button
          // disabled={!Boolean(target && target.material.map)}
          ref={removeTextureBtnRef}
          onClick={() => {
            removeTextureHandler("color");
          }}
        >
          Remove Texture
        </button>
      </div>

      <div className="texture">
        <span>Normals</span>
        <input
          accept=".jpg, .png"
          onChange={(e) => {
            changeTextureHandler(e, "normal");
          }}
          ref={normalRef}
          type="file"
        />
        <button
          onClick={() => {
            textureBtnHandler("normal");
          }}
          disabled={!Boolean(target)}
        >
          Load Normals
        </button>
        <button
          // disabled={!Boolean(target && target.material.normalMap)}
          ref={removeNormalBtnRef}
          onClick={() => {
            removeTextureHandler("normal");
          }}
        >
          Remove Normals
        </button>
      </div>
    </>
  );
};

export default ChangeModelTexture;
