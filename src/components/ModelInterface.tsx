import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { RepeatWrapping, TextureLoader } from "three";
import { MyMesh } from "../store/modelSlice";
import { RootState } from "../store/store";
import ChangeEnvMap from "./ChangeEnvMap";
import ChangeModelParams from "./ChangeModelParams";
import ChangeModelTexture from "./ChangeModelTexture";
import { Experience } from "../Experience/Experience";

type TextureType = "color" | "normal";

interface Props {
  exp: Experience | null;
}

const ModelInterface = ({ exp }: Props) => {
  const [roughness, setRoughness] = useState(0);
  const [opacity, setOpacity] = useState(0);
  const [metalness, setMetalness] = useState(0);

  const modelName = useSelector<RootState, string>((state) => state.model.modelName);
  const target = useSelector<RootState, MyMesh | undefined>(
    (state) => state.model.currTarget.target
  );

  return (
    <div className="model-change">
      <h2>Current Model:</h2>
      <p>{modelName}</p>
      <div className="target-options">
        <ChangeModelParams
          target={target}
          opacity={opacity}
          metalness={metalness}
          roughness={roughness}
          onMetalnessChange={setMetalness}
          onOpacityChange={setOpacity}
          onRoughnessChange={setRoughness}
        />
        <ChangeModelTexture target={target} />
        <ChangeEnvMap exp={exp} target={target} />
      </div>
    </div>
  );
};

export default ModelInterface;
