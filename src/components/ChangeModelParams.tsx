import { useRef, useEffect } from "react";
import { Slider } from "@mui/material";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";
import { MyMesh } from "../store/modelSlice";
import { Color } from "three";

interface Props {
  opacity: number;
  onOpacityChange(val: number): void;
  metalness: number;
  onMetalnessChange(val: number): void;
  roughness: number;
  onRoughnessChange(val: number): void;
  target?: MyMesh;
}

const ChangeModelParams = ({
  opacity,
  metalness,
  roughness,
  onMetalnessChange,
  onOpacityChange,
  onRoughnessChange,
  target,
}: Props) => {
  // const target = useSelector<RootState, MyMesh | undefined>(
  //   (state) => state.model.currTarget.target
  // );

  const colorRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (target) {
      if (colorRef.current) {
        let color = target.material.color.getStyle();
        colorRef.current.style.backgroundColor = color;
      }

      onRoughnessChange(target.material.roughness);
      onOpacityChange(target.material.opacity);
      onMetalnessChange(target.material.metalness);
    } else {
      if (colorRef.current) colorRef.current.style.backgroundColor = "#fff";
      onRoughnessChange(0);
      onOpacityChange(0);
      onMetalnessChange(0);
    }
  }, [target]);

  const changeColorHandler = () => {
    if (!target || !colorRef?.current) return;
    let newColor = new Color().setRGB(Math.random(), Math.random(), Math.random());

    target.material.color = newColor;
    colorRef.current.style.backgroundColor = newColor.getStyle();
  };

  let changeRoughnessHandler = () => {
    if (!target) return;
    target.material.roughness = roughness;
    target.material.needsUpdate = true;
  };

  let changeOpacityHandler = () => {
    if (!target) return;
    target.material.opacity = opacity;
    if (!target.material.transparent) target.material.transparent = true;
    target.material.needsUpdate = true;
  };
  let changeMetalnessHandler = () => {
    if (!target) return;
    target.material.metalness = metalness;
    target.material.needsUpdate = true;
  };

  return (
    <>
      <div className="color">
        <span className="title">Color:</span>
        <div onClick={changeColorHandler} ref={colorRef} className="palette"></div>
      </div>
      <div className="roughness">
        <span>Roughness</span>
        <Slider
          size="small"
          max={1}
          min={0}
          step={0.01}
          aria-label="Small"
          disabled={!Boolean(target)}
          onChangeCommitted={changeRoughnessHandler}
          onChange={(e, value) => onRoughnessChange(value as number)}
          value={!!roughness === false ? 0 : roughness}
          valueLabelDisplay="auto"
        />
      </div>
      <div className="opacity">
        <span>Opacity</span>
        <Slider
          size="small"
          max={1}
          min={0}
          step={0.01}
          aria-label="Small"
          disabled={!Boolean(target)}
          onChangeCommitted={changeOpacityHandler}
          onChange={(e, value) => onOpacityChange(value as number)}
          value={!!opacity === false ? 0 : opacity}
          valueLabelDisplay="auto"
        />
      </div>
      <div className="metalness">
        <span>Metalness</span>
        <Slider
          size="small"
          max={1}
          min={0}
          step={0.01}
          aria-label="Small"
          disabled={!Boolean(target)}
          onChangeCommitted={changeMetalnessHandler}
          onChange={(e, value) => onMetalnessChange(value as number)}
          value={!!metalness === false ? 0 : metalness}
          valueLabelDisplay="auto"
        />
      </div>
    </>
  );
};

export default ChangeModelParams;
