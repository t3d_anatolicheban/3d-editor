import React, { useEffect, useRef, useState } from "react";
import { store } from "../store/store";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../store/store";
import { modelActions } from "../store/modelSlice";
import ModelInterface from "./ModelInterface";
import { Experience } from "../Experience/Experience";

interface Props {
  exp: Experience | null;
}

const Interface = ({ exp }: Props) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const height = useSelector<RootState, number>((state) => state.model.sizes.h);
  const width = useSelector<RootState, number>((state) => state.model.sizes.w);
  const modelPath = useSelector<RootState, string>((state) => state.model.modelPath);
  const dispatch = useDispatch();

  const changeHeightHandler = (height: string) => {
    dispatch(modelActions.changeHeight(+height));
  };

  const changeWidthHandler = (width: string) => {
    dispatch(modelActions.changeWidth(+width));
  };

  const loadButtonlHandler = () => {
    inputRef.current?.click();
  };

  const loadModelHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    if (modelPath) URL.revokeObjectURL(modelPath);
    if (!e.target.files?.length) return;
    let path = URL.createObjectURL(e.target.files?.[0] as File);

    dispatch(
      modelActions.setModelInfo({
        name: e.target.files[0].name,
        path,
      })
    );
  };

  return (
    <aside className="interface">
      <h2>3D Editor</h2>
      <div className="plane-sizes">
        <h3>Plane sizes</h3>
        <input
          type="number"
          value={height}
          onChange={(e) => changeHeightHandler(e.target.value)}
          max="100"
          min={"1"}
          placeholder="Height"
        />
        <input
          type="number"
          value={width}
          onChange={(e) => changeWidthHandler(e.target.value)}
          max="100"
          min={"1"}
          placeholder={"Width"}
        />
      </div>
      <div className="model-input">
        <input onChange={loadModelHandler} ref={inputRef} type="file" />
        <button onClick={loadButtonlHandler}>Load Model</button>
      </div>
      <ModelInterface exp={exp} />
    </aside>
  );
};

export default Interface;
