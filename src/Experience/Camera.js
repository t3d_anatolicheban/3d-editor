import { Experience } from "./Experience";
import { PerspectiveCamera, Vector3 } from "three";
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
export class Camera {
  constructor() {
    this.experience = new Experience()

    this.setInstance()
    this.setControls()
  }

  setInstance() {
    this.instance = new PerspectiveCamera(35, this.experience.sizes.width / this.experience.sizes.height, .1, 1000)
    this.instance.position.set(5, 5, 7)
    this.experience.scene.add(this.instance)
  }

  resize() {
    this.instance.aspect = this.experience.sizes.width / this.experience.sizes.height
    this.instance.updateProjectionMatrix()
  }

  setControls() {
    this.controls = new OrbitControls(this.instance, this.experience.canvas)
    this.controls.enableDamping = true
  }



  update() {
    this.controls.update()
  }
}