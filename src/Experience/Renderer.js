import { Experience } from "./Experience";
import * as THREE from 'three'
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";
import { OutlinePass } from "three/examples/jsm/postprocessing/OutlinePass";
import { Vector2 } from "three";

export default class Renderer {
  constructor() {
    this.experience = new Experience()
    this.canvas = this.experience.canvas
    this.sizes = this.experience.sizes
    this.camera = this.experience.camera.instance
    this.scene = this.experience.scene
    // this.world = this.experience.world
    // this.raycaster = this.experience.raycaster

    this.setInstance()
  }

  setInstance() {
    this.instance = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: true })

    this.instance.useLegacyLights = true
    this.instance.outputEncoding = THREE.sRGBEncoding
    this.instance.toneMapping = THREE.CineonToneMapping
    this.instance.toneMappingExposure = 1.75
    this.instance.shadowMap.enabled = true
    this.instance.shadowMap.type = THREE.PCFSoftShadowMap
    this.instance.setClearColor('#26abff')
    this.instance.setSize(this.sizes.width, this.sizes.height)
    this.instance.setPixelRatio(this.sizes.pixelRatio)

    this.composer = new EffectComposer(this.instance)

    this.renderPass = new RenderPass(this.scene, this.camera)
    this.composer.addPass(this.renderPass)

    this.outlinePass = new OutlinePass(
      new Vector2(window.innerWidth, window.innerHeight), this.scene, this.camera
    )
    this.composer.addPass(this.outlinePass)
  }

  resize() {
    this.instance.setSize(this.sizes.width, this.sizes.height)
    this.instance.setPixelRatio(this.sizes.pixelRatio)
  }

  update() {
    // this.instance.render(this.experience.scene, this.camera)
    this.composer.render()
  }
}