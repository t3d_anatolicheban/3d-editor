import { Raycaster, Vector2 } from "three";
import { modelActions } from "../store/modelSlice";
import { store } from "../store/store";
import { Experience } from "./Experience";

export class MyRaycaster {
  constructor() {
    this.experience = new Experience()
    this.sizes = this.experience.sizes
    this.camera = this.experience.camera.instance
    this.instance = new Raycaster()
    this.model = this.experience.world.model

    this.NDCoords = new Vector2()

    this.setListeners()

    //
    this.dragged = false
    this.mouseDown = false
  }

  setListeners() {
    this.experience.canvas.addEventListener('mousedown', () => {
      this.dragged = false
      this.mouseDown = true
    })

    this.experience.canvas.addEventListener('click', () => {
      if (this.dragged) {
        this.dragged = false
        this.mouseDown = false
        return
      }
      if (!this.experience.world?.model?.object) return
      this.model = this.experience.world.model

      this.instance.setFromCamera(this.NDCoords, this.camera)
      let intersect = this.instance.intersectObject(this.experience.world.model.object)

      if (intersect.length && !this.model.transformControls.userData.dragged) {
        store.dispatch(modelActions.setTarget(intersect[0].object))
        this.model.setActiveMesh(intersect[0].object)
      } else if (!this.model.transformControls.userData.dragged) {
        this.model.removeActiveMesh()
        store.dispatch(modelActions.removeTarget())
      }
      this.model.transformControls.userData.dragged = false
      this.mouseDown = false
    })

    this.experience.canvas.addEventListener('mousemove', (e) => {
      this.NDCoords.x = e.clientX / this.sizes.width * 2 - 1
      this.NDCoords.y = - e.clientY / this.sizes.height * 2 + 1

      if (this.mouseDown) this.dragged = true
    })
  }
}