import { store } from "../../store/store";
import { Experience } from "../Experience";
import { Lights } from "./Lights";
import { Model } from "./Model";
import { Plane } from "./Plane";

export class World {
  constructor() {
    this.experience = new Experience()
    this.scene = this.experience.scene

    this.state = store.getState()

    this.setPlane()

    this.lights = new Lights()
  }

  setPlane() {
    this.state = store.getState()
    if (this.plane) {
      this.scene.remove(this.plane.mesh)
      this.plane.material.dispose()
      this.plane.geometry.dispose()
    }

    this.plane = new Plane(this.state.model.sizes.h, this.state.model.sizes.w)
  }

  setModel() {
    if (this.model) {
      this.model.transformControls.detach()
      this.scene.remove(this.model.object)
    }
    this.model = new Model()
  }

}