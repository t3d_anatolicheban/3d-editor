import { AmbientLight, PointLight } from "three";
import { Experience } from "../Experience";

export class Lights {
  constructor() {
    this.experience = new Experience()

    this.ambLight = new AmbientLight('#fff', .7)

    this.pointLight = new PointLight('#fff', 5)
    this.pointLight.position.set(10, 20, 5)

    this.experience.scene.add(this.ambLight, this.pointLight)
  }
}