import { BackSide, Box3, Group, Mesh, MeshBasicMaterial, MeshStandardMaterial, ObjectLoader, Vector3 } from "three";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
import { store } from "../../store/store";
import { Experience } from "../Experience";
import { TransformControls } from 'three/examples/jsm/controls/TransformControls'

export class Model {
  constructor() {
    this.state = store.getState()
    if (!this.state.model.modelPath) {
      return
    }
    this.experience = new Experience()

    this.modelExt = this.state.model.modelName.split('.').pop()

    this.object = null
    this.activeMesh = null
    this.loader = null

    this.transformControls = new TransformControls(this.experience.camera.instance, this.experience.canvas)
    this.transformControls.addEventListener('dragging-changed', (e) => {
      this.experience.camera.controls.enabled = !e.value
      this.transformControls.userData.dragged = true
    })
    this.experience.scene.add(this.transformControls)

    switch (this.modelExt) {
      case 'obj':
        this.setOBJModel()
        break
      case 'gltf':
        this.setGLTFModel()
        break
      case 'glb':
        this.setGLTFModel()
        break
      case 'fbx':
        this.setFBXModel()
        break
      default:
        console.error('Invalid file extention!')
    }
  }

  setGLTFModel() {
    this.loader = new GLTFLoader()
    let draco = new DRACOLoader()
    this.loader.setDRACOLoader(draco)

    this.loader.load(this.state.model.modelPath, (gltf) => {
      this.object = gltf.scene
      this.centerModel(this.object)
      this.traverseModel(this.object)
      this.experience.scene.add(this.object)
    }, () => { },
      () => {
        console.error('Failed to load!');
      }
    )
  }

  setOBJModel() {
    this.loader = new OBJLoader()

    this.loader.load(this.state.model.modelPath, (obj) => {
      this.object = obj
      this.centerModel(this.object)
      this.traverseModel(this.object)
      this.experience.scene.add(this.object)
    }, () => { },
      () => {
        console.error('Failed to load!');
      }
    )
  }

  setFBXModel() {
    this.loader = new FBXLoader()

    this.loader.load(this.state.model.modelPath, (fbx) => {
      this.object = fbx
      this.centerModel(this.object)
      this.traverseModel(this.object)
      this.experience.scene.add(this.object)
    }, () => { },
      () => {
        console.error('Failed to load!');
      }
    )
  }

  centerModel(model) {
    let box3 = new Box3()
    box3.setFromObject(this.object)
    let center = new Vector3()
    box3.getCenter(center)
    let size = new Vector3()
    box3.getSize(size)
    model.position.set(-center.x, -center.y, -center.z)
    model.position.y += size.y / 2 + .01
  }

  traverseModel(model) {
    model.traverse(child => {
      // console.log(child);
      if (child instanceof Mesh && !(child.material instanceof MeshStandardMaterial)) {
        let { opacity, color, } = child.material
        child.material = new MeshStandardMaterial({ opacity, roughness: .5, color, metalness: .25 })
        // console.log(child);
      }
    })
  }

  setActiveMesh(mesh) {
    if (this.activeMesh) this.activeMesh = null
    this.activeMesh = mesh
    this.transformControls.attach(this.activeMesh)
    this.experience.renderer.outlinePass.selectedObjects = [this.activeMesh]
  }

  removeActiveMesh() {
    this.transformControls.detach()
    this.activeMesh = null
    this.experience.renderer.outlinePass.selectedObjects = []
  }
}