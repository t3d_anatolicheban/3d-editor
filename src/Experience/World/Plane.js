import { Mesh, MeshBasicMaterial, PlaneGeometry } from "three";
import { Experience } from "../Experience";

export class Plane {
  constructor(w, h) {

    this.experience = new Experience()

    this.geometry = new PlaneGeometry(w, h)
    this.material = new MeshBasicMaterial({ color: '#808080' })

    this.mesh = new Mesh(this.geometry, this.material)

    this.mesh.rotation.x = -Math.PI / 2
    this.experience.scene.add(this.mesh)
  }
}