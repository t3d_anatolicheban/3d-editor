import { Scene } from "three";
import { modelActions } from "../store/modelSlice";
import { store } from "../store/store";
import { Camera } from "./Camera";
import { MyRaycaster } from "./MyRaycaster";
import Renderer from "./Renderer";
import { sources } from "./sources";
import { Resourses } from "./Utils/Resourses";
import { Sizes } from "./Utils/Sizes";
import { Time } from "./Utils/Time";
import { World } from "./World/World";

let instance;

export class Experience {

  constructor(canvas) {

    if (instance) {
      return instance
    }

    instance = this

    //Options
    this.canvas = canvas

    //Redux staff
    this.state = store.getState()

    //Setup
    this.wrapper = document.querySelector('.canvas-wrapper')
    this.sizes = new Sizes()
    this.scene = new Scene()
    this.time = new Time()
    this.camera = new Camera()
    this.renderer = new Renderer()
    // this.resources = new Resourses(sources)
    this.world = new World()
    this.raycaster = new MyRaycaster()

    this.sizes.on('resize', () => {
      this.resize()
    })

    this.time.on('tick', () => {
      this.update()
    })

    store.subscribe(() => {
      let state = store.getState()
      if (state.model.sizes.h !== this.state.model.sizes.h || state.model.sizes.w !== this.state.model.sizes.w) {
        this.world.setPlane()
      }
      if (state.model.modelPath !== this.state.model.modelPath) {
        this.world.setModel()
      }
      this.state = state
    })
  }

  resize() {
    this.camera.resize()
    this.renderer.resize()
  }

  update() {
    this.renderer.update()
    this.camera.update()
  }

  logger() {
    console.log(this);
  }

  setEnvMap(texture, type) {

  }
}
