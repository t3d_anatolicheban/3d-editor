export const sources = [
  {
    name: 'fire',
    path: './fire.jpg',
    type: 'texture'
  },
  {
    name: 'ground',
    path: './ground.jpg',
    type: 'texture'
  },
  {
    name: 'water',
    path: './water.jpg',
    type: 'texture'
  },
];
