import { createSlice } from "@reduxjs/toolkit";
import { PayloadAction } from "@reduxjs/toolkit";
import { BufferGeometry, Color, Mesh, MeshStandardMaterial } from "three";
// import { Texture as ThreeTexture } from "three";
import { Experience } from "../Experience/Experience";

export type Texture = "water" | "fire" | "ground" | "none";
export type MyMesh = Mesh<BufferGeometry, MeshStandardMaterial>;
export type MyExperience = NonNullable<Experience>;

interface ModelState {
  sizes: { w: number; h: number };
  modelName: string;
  modelPath: string;
  currTarget: {
    texture: Texture;
    target?: NonNullable<MyMesh>;
  };
}

let initState: ModelState = {
  sizes: { w: 10, h: 10 },
  modelName: "No model",
  modelPath: "",
  currTarget: {
    texture: "none",
    target: undefined,
  },
};

export const modelSlice = createSlice({
  name: "model",
  initialState: initState,
  reducers: {
    changeHeight: (state, action: PayloadAction<number>) => {
      state.sizes.h = action.payload;
    },
    changeWidth: (state, action: PayloadAction<number>) => {
      state.sizes.w = action.payload;
    },
    setTarget: (state, action: PayloadAction<MyMesh>) => {
      state.currTarget.target = action.payload;
    },
    removeTarget: (state, action) => {
      state.currTarget.target = undefined;
    },
    setModelInfo: (state, action: PayloadAction<{ name: string; path: string }>) => {
      state.modelName = action.payload.name;
      state.modelPath = action.payload.path;
      state.currTarget.target = undefined;
    },
  },
});

export const { reducer: ModelReducer, actions: modelActions } = modelSlice;
