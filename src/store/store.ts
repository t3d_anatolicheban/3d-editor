import { configureStore } from "@reduxjs/toolkit";
import { ModelReducer } from "./modelSlice";

export const store = configureStore({
  reducer: {
    model: ModelReducer,
  },
  middleware: (getDef) => getDef(),
  devTools: true,
});

export type RootState = ReturnType<typeof store.getState>;
