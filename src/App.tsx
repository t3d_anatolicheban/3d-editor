import React, { useState } from "react";
import Canvas from "./components/Canvas";
import Interface from "./components/Interface";
import { Experience } from "./Experience/Experience";

const App = () => {
  let [experience, setExperience] = useState<Experience | null>(null);

  let expCreatedHandler = (exp: Experience) => {
    setExperience(exp);
  };

  return (
    <>
      <Canvas onExpCreated={expCreatedHandler} />
      <Interface exp={experience} />
    </>
  );
};

export default App;
